export function validation(e, validations) {
    const { value, type, name } = e.target;
    const result = {
        name: name,
        valid: true,
        errorMessage: ""
    };

    validations.forEach(validation => {
        _validate(name, value, validation);
    });

    function _validate(name, value, validation) {
        if (result.valid) {
            switch (validation.type) {
                case "required":
                    if (!value.length) {
                        result.errorMessage = name + " must required!";
                        result.valid = false;
                    }
                    break;

                case "email":
                    var regex = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
                    var errorMessage = value + " is not an email";
                    _checkWithRegex(value, regex, errorMessage);
                    break;

                case "number":
                    regex = /^[0-9]{5,15}$/;
                    errorMessage = value + " must be  a number";
                    _checkWithRegex(value, regex, errorMessage);
                    break;

                case "length":
                    if (value.length < validation.min || value.length > validation.max) {
                        result.errorMessage = name + " must has length in range " + validation.min + " - " + validation.max;
                        result.valid = false;
                    }
                    break;

                // case "password":
                //     const regPassword = /^\w{8,}$/;
                //     if (pattern) {
                //         showValid(name, value, pattern);
                //     } else {
                //         showValid(name, value, regPassword);
                //     }
                //     break;

                // case "file":
                //     const regImg = /\.(jpg|svg|jpeg|png|bmp|gif)$/i;
                //     if (regImg.test(name)) {
                //         validate.errorText = "";
                //         validate.valid = false;
                //     } else {
                //         validate.errorText = name + " Not in the correct format !";
                //         validate.valid = true;
                //     }
                //     break;
                default:
                    break;
            }
        }

    }

    function _checkWithRegex(value, regex, errorMessage) {
        if (!regex.test(value)) {
            result.errorMessage = errorMessage;
            result.valid = false;
        }
    }

    return result;
}